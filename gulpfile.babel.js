import gulp from 'gulp'

import del from 'del'
import concat from 'gulp-concat'
import sourcemaps from 'gulp-sourcemaps'
import stylus from 'gulp-stylus'
import html from 'gulp-htmlmin'
import babel from 'gulp-babel'
import browserSync from 'browser-sync'

import kouto from 'kouto-swiss'
import jeet from 'jeet'
import typographic from 'typographic'

const browser = browserSync.create()

const dirs = {
  src: './src',
  dist: './dist'
}

const javascriptPaths = {
  src: `${dirs.src}/javascript/**/*.js`,
  dist: `${dirs.dist}/js`
}

const stylusPaths = {
  src: `${dirs.src}/stylus/main.styl`,
  changes: `${dirs.src}/stylus/**/*`,
  dist: `${dirs.dist}/css/`
}

const htmlPaths = {
  src: `${dirs.src}/html/*.html`,
  dist: dirs.dist
}

gulp.task('browsersync', function() {
  browser.init({
    server: {
      baseDir: dirs.dist
    }
  })
})

gulp.task('bs-reload', function() {
  return browser.reload()
});

gulp.task('styles', () => {
  return gulp.src(stylusPaths.src)
    .pipe(sourcemaps.init())
    .pipe(stylus({
      use: [kouto(), jeet(), typographic()]
    }))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest(stylusPaths.dist))
})

gulp.task('scripts', () => {
  return gulp.src(javascriptPaths.src)
    .pipe(sourcemaps.init())
    .pipe(babel())
		.pipe(concat('main.js'))
		.pipe(sourcemaps.write('.'))
		.pipe(gulp.dest(javascriptPaths.dist))
})

gulp.task('html', () => {
  return gulp.src(htmlPaths.src)
    .pipe(html({ collapseWhitespace: true }))
    .pipe(gulp.dest(htmlPaths.dist))
})

gulp.task('img', () => {
  return gulp.src(`${dirs.src}/img/**/*`)
    .pipe(gulp.dest(`${dirs.dist}/img`))
})

gulp.task('clean', () => {
  return del([dirs.dist])
})

gulp.task('default', ['clean', 'styles', 'scripts', 'img', 'html', 'browsersync'], () => {
  gulp.watch(`${dirs.src}/img/**/*`, ['img'])
  gulp.watch(stylusPaths.changes, ['styles'])
  gulp.watch(javascriptPaths.src, ['scripts', 'bs-reload'])
  gulp.watch(htmlPaths.src, ['html', 'bs-reload'])
})
